package com.twuc.webApp.domain.mazeRender;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 代表了迷宫的渲染网格。和 {@link com.twuc.webApp.domain.mazeGenerator.Grid} 不同。这个网格包含了额外的渲染信息。
 * 而前者仅仅包含路径信息。
 */
public class RenderCell {
    private final int row;
    private final int column;
    private HashMap<String, Object> tags;

    private RenderCell north;
    private RenderCell south;
    private RenderCell east;
    private RenderCell west;

    private RenderType renderType;
    private int direction;

    /**
     * 创建一个 {@link RenderCell} 实例。
     * @param row 该渲染网格包含的行数。该行数既包含路径节点又包含非路径节点。
     * @param column 该渲染网格包含的列数。该列数既包含路径节点又包含非路径节点。
     */
    public RenderCell(int row, int column) {
        this.row = row;
        this.column = column;
    }

    /**
     * 设置该渲染节点的邻居节点。
     *
     * @param north 位于该节点北方的节点。如果不存在则为 {@code null}。
     * @param south 位于该节点南方的节点。如果不存在则为 {@code null}。
     * @param east 位于该节点东方的节点。如果不存在则为 {@code null}。
     * @param west 位于该节点西方的节点。如果不存在则为 {@code null}。
     */
    public void setNeighbors(RenderCell north, RenderCell south, RenderCell east, RenderCell west) {
        this.north = north;
        this.south = south;
        this.east = east;
        this.west = west;
    }

    /**
     * 获得该节点的邻居节点。
     *
     * @return 邻居节点集合。
     */
    public List<RenderCell> getNeighbors() {
        return Stream.of(north, south, east, west)
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    /**
     * 获得当前渲染节点所在的行号（从 0 开始）。
     *
     * @return 当前渲染节点所在的行号（从 0 开始）。
     */
    public int getRow() {
        return row;
    }

    /**
     * 获得当前渲染节点所在的列号（从 0 开始）。
     *
     * @return 当前渲染节点所在的列号（从 0 开始）。
     */
    public int getColumn() {
        return column;
    }

    /**
     * 获得该节点北方的节点。
     *
     * @return 该节点北方的节点。如果不存在则为 {@code null}。
     */
    public RenderCell getNorth() {
        return north;
    }

    /**
     * 获得该节点南方的节点。
     *
     * @return 该节点南方的节点。如果不存在则为 {@code null}。
     */
    public RenderCell getSouth() {
        return south;
    }

    /**
     * 获得该节点东方的节点。
     *
     * @return 该节点东方的节点。如果不存在则为 {@code null}。
     */
    public RenderCell getEast() {
        return east;
    }

    /**
     * 获得该节点西方的节点。
     *
     * @return 该节点西方的节点。如果不存在则为 {@code null}。
     */
    public RenderCell getWest() {
        return west;
    }

    /**
     * 获得该节点的渲染类型。
     *
     * @return 该节点的渲染类型。
     */
    public RenderType getRenderType() {
        return renderType;
    }

    /**
     * 设置该节点的渲染类型。
     *
     * @param renderType 该节点的渲染类型。
     */
    public void setRenderType(RenderType renderType) {
        this.renderType = renderType;
    }

    /**
     * 获得该节点的连通性（这个连通性仅仅影响渲染）。
     *
     * @return 该节点的连通性。可进行按位操作。
     */
    public int getDirection() {
        return direction;
    }

    /**
     * 设置该节点的连通性（这个连通性仅仅影响渲染）。
     *
     * @param direction 该节点的连通性。可进行按位操作。
     */
    public void setDirection(int direction) {
        this.direction = direction;
    }

    /**
     * 获得该渲染节点的附加数据。
     *
     * @param key 该附加数据的索引。
     * @return 该附加数据的值。
     */
    public Object getTag(String key) {
        if (key == null) return null;
        HashMap<String, Object> theTags = tags;
        if (theTags == null) return null;
        return theTags.getOrDefault(key, null);
    }

    void setTags(Map<String, Object> cellTags) {
        tags = new HashMap<>(cellTags);
    }
}
